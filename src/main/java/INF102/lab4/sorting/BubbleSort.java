package INF102.lab4.sorting;

import java.util.List;

public class BubbleSort implements ISort {

    @Override
    public <T extends Comparable<T>> void sort(List<T> list) {
        int n = list.size();
        boolean shift;
        int i, j;

        for(i = 0; i < n - 1; i++){
            shift = false;
            for (j = 0; j < n - i - 1; j++){
                if (list.get(j).compareTo(list.get(j + 1)) > 0){

                    //Skifter plass på de to elementene hvis j er større enn j+1
                    T current = list.get(j);
                    list.set(j, list.get(j + 1));
                    list.set(j + 1, current);
                    shift = true;
                }

            }
            //Hvis løkken går gjennom listen uten å skifte plass på noen elementer, så er listen sortert
            if (shift != true){
                break;
            }
            
        }
    }
    
}
