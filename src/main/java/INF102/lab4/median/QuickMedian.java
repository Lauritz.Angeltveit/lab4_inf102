package INF102.lab4.median;

import java.util.ArrayList;
import java.util.List;

public class QuickMedian implements IMedian {

    @Override
    public <T extends Comparable<T>> T median(List<T> list) {
        List<T> listCopy = new ArrayList<>(list); // Method should not alter list
        int n = listCopy.size();
        return medianFinder(listCopy, 0, n - 1);
    }

    private <T extends Comparable<T>> T medianFinder(List<T> listCopy, int L, int R){
        if(L == R){
            return listCopy.get(L);
        }

        int p = partition(listCopy, L, R);
        int k = listCopy.size() / 2;


        if(k == p){
            return listCopy.get(p);
        }
        else if(k < p){
            return medianFinder(listCopy, L, p - 1);
        }
        else {
            return medianFinder(listCopy, p + 1, R);
        }
        
    }

    private <T extends Comparable<T>> int partition(List<T> listCopy, int L, int R) {
        int i = L - 1;
        T piv = listCopy.get(R);

        for(int j = L; j < R; j++){
            if(listCopy.get(j).compareTo(piv)<=0){
                i++;
                T current = listCopy.get(i);
                listCopy.set(i, listCopy.get(j));
                listCopy.set(j, current);
            }
        }
        T current = listCopy.get(i+1);
        listCopy.set(i + 1, listCopy.get(R));
        listCopy.set(R, current);
        return i + 1;
    }
}
